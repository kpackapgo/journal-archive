package com.ee.journalarchive.controller;

import com.ee.journalarchive.JournalArchiveApplication;
import com.ee.journalarchive.entry.CaseFile;
import com.ee.journalarchive.entry.Classification;
import com.ee.journalarchive.entry.JournalEntry;
import com.ee.journalarchive.entry.Record;
import com.ee.journalarchive.entryDTO.ClassificationDTO;
import com.ee.journalarchive.entryDTO.JournalEntryDTO;
import com.ee.journalarchive.service.JournalEntryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = JournalArchiveApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class JournalEntryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JournalEntryService journalEntryService;

    @Test
    public void entryIsStored() throws Exception {
        JournalEntryDTO journalEntryDTO = new JournalEntryDTO();
        ClassificationDTO classificationDTO = new ClassificationDTO();
        classificationDTO.setClassIdent("1");
        classificationDTO.setTitle("Some title");
        journalEntryDTO.setClassification(classificationDTO);
        ObjectMapper mapper = new ObjectMapper();
        String userAsJson = mapper.writeValueAsString(journalEntryDTO);
        mockMvc.perform(post("/api/entry").contentType(MediaType.APPLICATION_JSON).content(userAsJson)).andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void entriesAreRetrieved() throws Exception {
        JournalEntry journalEntry = new JournalEntry();
        Classification classification = new Classification();
        classification.setClassIdent("1");
        classification.setTitle("Some title");
        journalEntry.setClassification(classification);
        CaseFile caseFile = new CaseFile();
        caseFile.setTitle("The case file title is real");
        caseFile.setPublicTitle("The case file title is ****");
        Record record = new Record();
        record.setTitle("The record title is real");
        record.setPublicTitle("The record title is ****");
        journalEntry.setCaseFile(caseFile);
        journalEntry.setRecord(record);

        when(journalEntryService.getEntries(1, 5)).thenReturn(Lists.newArrayList(journalEntry));

        mockMvc.perform(get("/api/entry").contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(1)))
                .andExpect(jsonPath("$[0].caseFile.title", is("The case file title is ****")))
                .andExpect(jsonPath("$[0].record.title", is("The record title is ****")));
    }

    @Test
    public void parametersSpecifiedInTheRequestAreUsedInstead() throws Exception {
        JournalEntry journalEntry = new JournalEntry();
        Classification classification = new Classification();
        journalEntry.setClassification(classification);
        CaseFile caseFile = new CaseFile();
        Record record = new Record();
        journalEntry.setCaseFile(caseFile);
        journalEntry.setRecord(record);

        mockMvc.perform(get("/api/entry?page=2&pageSize=13").contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk());

        verify(journalEntryService, times(1)).getEntries(2,13);
    }
}