package com.ee.journalarchive.entryDTO;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class RecordDTO implements Serializable {

    private int recordYear;
    private int recordSequenceNumber;
    private int recordNumber;
    private String title;
    private String publicTitle;
    private String recordDate;
    private List<PartyDTO> parties;
}
