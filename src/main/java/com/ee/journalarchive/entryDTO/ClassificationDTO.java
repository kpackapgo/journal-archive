package com.ee.journalarchive.entryDTO;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ClassificationDTO implements Serializable {

    private String classIdent;
    private String title;
}
