package com.ee.journalarchive.entryDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CaseFileDTO implements Serializable {

    private int caseYear;
    private int caseSequenceNumber;
    private String title;
    private String publicTitle;
}
