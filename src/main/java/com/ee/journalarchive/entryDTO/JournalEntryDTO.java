package com.ee.journalarchive.entryDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonPropertyOrder({ "class", "caseFile", "record" })
public class JournalEntryDTO implements Serializable {

    @JsonProperty("class")
    private ClassificationDTO classification;
    private CaseFileDTO caseFile;
    private RecordDTO record;
}
