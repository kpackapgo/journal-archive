package com.ee.journalarchive.entryDTO;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PartyDTO implements Serializable {

    private String partyType;
    private String partyName;
}
