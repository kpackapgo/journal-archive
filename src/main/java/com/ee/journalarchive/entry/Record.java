package com.ee.journalarchive.entry;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Record {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int recordYear;
    private int recordSequenceNumber;
    private int recordNumber;
    private String title;
    private String publicTitle;
    private String recordDate;

    @OneToMany(fetch = FetchType.LAZY,  cascade = CascadeType.ALL)
    private List<Party> parties;
}
