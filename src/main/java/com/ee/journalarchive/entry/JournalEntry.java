package com.ee.journalarchive.entry;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class JournalEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY,  cascade = CascadeType.ALL)
    private Classification classification;

    @ManyToOne(fetch = FetchType.LAZY,  cascade = CascadeType.ALL)
    private CaseFile caseFile;

    @OneToOne(fetch = FetchType.LAZY,  cascade = CascadeType.ALL)
    private Record record;
}
