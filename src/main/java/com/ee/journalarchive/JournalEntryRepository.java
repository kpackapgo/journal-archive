package com.ee.journalarchive;

import com.ee.journalarchive.entry.JournalEntry;
import com.sun.istack.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JournalEntryRepository extends PagingAndSortingRepository<JournalEntry, Long> {

    Page<JournalEntry> findAll(Pageable page);
}
