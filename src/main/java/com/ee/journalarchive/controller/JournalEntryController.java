package com.ee.journalarchive.controller;

import com.ee.journalarchive.customexceptions.RequestLimitExceededException;
import com.ee.journalarchive.entry.JournalEntry;
import com.ee.journalarchive.entryDTO.JournalEntryDTO;
import com.ee.journalarchive.service.JournalEntryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping(value = "api")
public class JournalEntryController {

    private static final int UPLOAD_REQUEST_LIMIT = 4;
    
    private AtomicInteger count = new AtomicInteger(0);

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JournalEntryService journalEntryService;

    @PostMapping("/entry")
    @ResponseStatus(HttpStatus.CREATED)
    public void addEntry(@RequestBody JournalEntryDTO entryDTO) {
        synchronized (this) {
            if (count.getPlain() == UPLOAD_REQUEST_LIMIT) {
                throw new RequestLimitExceededException("Active save requests limit is reached.");
            } else {
                System.out.println(count.incrementAndGet());
            }
        }

        JournalEntry journalEntry = convertToEntity(entryDTO);
        journalEntryService.saveEntry(journalEntry);

        count.decrementAndGet();
    }

    @GetMapping("/entry")
    public List<JournalEntryDTO> getEntries(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "5") int pageSize, @RequestParam(name = "public", defaultValue = "true") boolean isPublic) {
        List<JournalEntry> entries = journalEntryService.getEntries(page, pageSize);
        List<JournalEntryDTO> dtos = new ArrayList<>();
        for (JournalEntry entry : entries) {
            dtos.add(convertToDTO(entry, isPublic));
        }

        return dtos;
    }

    private JournalEntry convertToEntity(JournalEntryDTO journalEntryDTO) {

        return modelMapper.map(journalEntryDTO, JournalEntry.class);
    }

    private JournalEntryDTO convertToDTO(JournalEntry journalEntry, boolean isPublic) {
        JournalEntryDTO journalEntryDTO = modelMapper.map(journalEntry, JournalEntryDTO.class);
        if (isPublic) {
            journalEntryDTO.getCaseFile().setTitle(journalEntry.getCaseFile().getPublicTitle());
            journalEntryDTO.getRecord().setTitle(journalEntry.getRecord().getPublicTitle());
        }

        return journalEntryDTO;
    }
}
