package com.ee.journalarchive.service;

import com.ee.journalarchive.JournalEntryRepository;
import com.ee.journalarchive.entry.JournalEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JournalEntryServiceImpl implements JournalEntryService {

    private JournalEntryRepository journalEntryRepository;

    @Autowired
    public JournalEntryServiceImpl(JournalEntryRepository journalEntryRepository) {
        this.journalEntryRepository = journalEntryRepository;
    }

    @Override
    public void saveEntry(JournalEntry entry) {
        journalEntryRepository.save(entry);
    }

    @Override
    public List<JournalEntry> getEntries(int page, int pageSize) {
        Page<JournalEntry> pageOJournalEntries = journalEntryRepository.findAll(PageRequest.of(page-1, pageSize));

        return pageOJournalEntries.getContent();
    }
}
