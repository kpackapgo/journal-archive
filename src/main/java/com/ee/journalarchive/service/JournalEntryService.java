package com.ee.journalarchive.service;

import com.ee.journalarchive.entry.JournalEntry;

import java.util.List;

public interface JournalEntryService {

    void saveEntry(JournalEntry entry);

    List<JournalEntry> getEntries(int page, int pageSize);
}
