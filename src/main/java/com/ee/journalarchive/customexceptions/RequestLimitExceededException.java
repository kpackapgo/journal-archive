package com.ee.journalarchive.customexceptions;

public class RequestLimitExceededException extends RuntimeException {

    public RequestLimitExceededException(String message) {
        super(message);
    }
}
