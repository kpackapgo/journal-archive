package com.ee.journalarchive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JournalArchiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(JournalArchiveApplication.class, args);
	}

}
