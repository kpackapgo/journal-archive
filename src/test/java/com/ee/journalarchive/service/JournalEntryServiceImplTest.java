package com.ee.journalarchive.service;

import com.ee.journalarchive.JournalEntryRepository;
import com.ee.journalarchive.entry.JournalEntry;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JournalEntryServiceImplTest {

    @Mock
    private JournalEntryRepository journalEntryRepository;

    @InjectMocks
    private JournalEntryServiceImpl journalEntryService;

    @Test
    public void entryIsSavedToTheDatabase() {

        JournalEntry entry = new JournalEntry();
        entry.setId(341L);

        journalEntryService.saveEntry(entry);

        verify(journalEntryRepository, times(1)).save(entry);
    }

    @Test
    public void pageOfEntriesIsRetrieve() {
        int page = 1;
        int pageSize = 5;
        JournalEntry entry = new JournalEntry();
        entry.setId(341L);
        List<JournalEntry> entryList = Lists.newArrayList(entry);
        Page<JournalEntry> pageOJournalEntries = new PageImpl<>(entryList);


        when(journalEntryRepository.findAll(PageRequest.of(0, pageSize))).thenReturn(pageOJournalEntries);

        List<JournalEntry> entriesResult = journalEntryService.getEntries(page, pageSize);

        assertEquals(entryList, entriesResult);
    }
}